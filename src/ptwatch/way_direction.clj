(ns ptwatch.way-direction
  [:use clojure.set])

(defn way-start
  [{nodes :nodes} direction]
  "Returns start node id, if treating way as having specified direction"
  (case direction
    :forward  (first nodes)
    :backward (peek nodes)))

(defn way-end
  [{nodes :nodes} direction]
  "Returns end node id, if treating way as having specified direction"
  (case direction
    :forward  (peek nodes)
    :backward (first nodes)))

(defn way-possible-starts
  [way possible-directions]
  (into #{} (for [dir possible-directions]
              (way-start way dir))))

(defn way-possible-ends
  [way possible-directions]
  (into #{} (for [dir possible-directions]
              (way-end way dir))))

(defn way-can-have-direction?
  [way direction possible-starts possible-ends]
  (and (or (nil? possible-starts) (contains? possible-starts (way-start way direction)))
       (or (nil? possible-ends) (contains? possible-ends (way-end way direction)))))

(defn way-possible-directions
  ([{oneway "oneway"}]
   (cond
    (#{"yes" "1" "true"} oneway) #{:forward}
    (#{"reversible" "-1" "reverse"} oneway) #{:backward}
    :else #{:forward :backward}))
  ([way possible-starts possible-ends]
   (set (filter
         #(way-can-have-direction? way % possible-starts possible-ends)
         (way-possible-directions way)))))

(defn directions-to-direction [directions]
  (case directions
    #{:forward :backward} :unknown
    #{:forward}           :forward
    #{:backward}          :backward))

;; Fixme: unify this to work without conversion
(defn direction-to-directions [direction]
  (case direction
    :unknown #{:forward :backward}
    :forward #{:forward}
    :backward #{:backward}))

(defn way-direction
  [possible-previous-nodes [way & [next-way & _ :as next-ways]]]
  {:post [(or (#{:forward :backward :unknown} %) (nil? %))]}
  """
  Returns direction of the way treating as it starting from possible nodes
  (can be nil if starting from any node, i.e. start of ways sequence).
  Returns nil if the way cannot be started from such nodes.
  Next ways are used to find out direction if pair has ambiguous direction
  (i.e. two ways starting and ending in the same node).
  """
  (case (way-possible-directions way possible-previous-nodes nil)
    #{:forward} :forward
    #{:backward} :backward
    #{:forward :backward} (if next-way
                            (let [next-way-direction (way-direction
                                                      (way-possible-ends way #{:forward :backward})
                                                      next-ways)]
                              (case next-way-direction
                                :unknown :unknown
                                nil :unknown
                                (directions-to-direction
                                 (way-possible-directions way
                                                          possible-previous-nodes
                                                          (way-possible-starts
                                                           next-way
                                                           #{next-way-direction})))))
                            :unknown)
    #{} nil))

(defn ways-directions
  [ways]
  "Returns list containing pairs [direction, way] or :break in break points"
  (or ((fn wd-with-nodes [possible-previous-nodes ways]
         (when-first [way ways]
           (if-let [dir (way-direction possible-previous-nodes ways)]
             (cons [dir way] (lazy-seq (wd-with-nodes (way-possible-ends way (direction-to-directions dir)) (rest ways))))
             (cons :break    (lazy-seq (wd-with-nodes nil ways))))))
       nil ways) '()))


