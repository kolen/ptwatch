
(ns ptwatch.osm
  (:require [clojure.xml]))

(defn- parse-id
  "Reads a number from a string. Returns nil if not a number."
  [s]
  (if (re-find #"^-?\d+\.?\d*$" s)
    (read-string s)))

(defn- parse-coord
  "Parse lat or lon" [s] (Double/parseDouble s))

(defn- parse-tags [el]
  (into {}
    (for [{tag :tag {k :k v :v} :attrs} (el :content)
          :when (= tag :tag)]
      [k v])))

(defmulti ^{:private true} parse-primitive-specific :tag)

(defmethod parse-primitive-specific :node [{{:keys [lat lon]} :attrs}]
  {:lat (parse-coord lat) :lon (parse-coord lon)})

(defmethod parse-primitive-specific :way [{:keys [content]}]
  {:nodes (into []
            (for [{tag :tag {ref :ref} :attrs} content
                  :when (= tag :nd)]
              (parse-id ref)))})

(defmethod parse-primitive-specific :relation [{:keys [content]}]
  {:members (into []
              (for [{tag :tag {:keys [type ref role]} :attrs} content
                    :when (= tag :member )]
                {:type (keyword type)
                 :ref (parse-id ref)
                 :role role}))})

(defn- parse-primitive-base [{:keys [attrs tag content] :as el}]
  (into (parse-tags el) {:type (keyword tag) :id (-> attrs :id parse-id)}))

(defn- parse-primitive [el]
  (into (parse-primitive-base el) (parse-primitive-specific el)))

(defn parse-xml [xml-source]
  "Parse OSM xml into primitive records"
  (let [{primitives-els :content} (clojure.xml/parse xml-source)]
    (for [{:keys [tag] :as el} primitives-els
          :when (#{:node :way :relation} tag)]
      (parse-primitive el))))

(defn primitive-pk [primitive]
  "Returns primary key of primitive, i.e. [:node 123456]"
  [(primitive :type) (or (primitive :id) (primitive :ref))])

(defn store-primitives [primitives]
  "Indexes supplied primitives by type and id and returns map"
  (into {} (for [p primitives] [(primitive-pk p) p])))

(defn deref-rel-member [storage member]
  "Return primitive referenced as relation member. Adds :role key"
  (conj (storage (primitive-pk member)) {:role (member :role)}))

(defn deref-rel-members [storage members-or-relation]
  """
  Lookup primitives from relation member references. Returns sequence of
  members, with added :role key. Both relation record or list of members
  from it can be supplied.
  """
  (let [members (if (map? members-or-relation)
                  (members-or-relation :members)
                  members-or-relation)]
    (for [m members] (deref-rel-member storage m))))
