(ns ptwatch.analyze
  [:use [clojure.set]])

(defn- member-type [{role :role}]
  (let [member-types {"stop" :stop
                      "stop_exit_only" :stop
                      "stop_entry_only" :stop
                      "platform" :platform
                      "platform_exit_only" :platform
                      "platform_entry_only" :platform
                      "" :road}]
    (member-types role)))

(defn- join-way-ends [ends next-starts next-ends]
  """
  Returns new possible way ends after joining new way to existing set of ends.
  Returns empty set if ways don't join
  """
  (let [shared-pts (intersection ends next-starts)]
    (case shared-pts
       #{} #{}
       (difference next-ends shared-pts))))

(defn- stops-to-pairs [[m1 m2 & ms]]
    (let [is-stop (some-> :role #{"stop" "stop-forward" "stop-backward"})
          is-platform (some-> :role #{"platform" "platform-forward" "platform-backward"})]
      (cond (nil? m1) []
            (is-stop m1)
              (cond (is-platform m2)
                    (lazy-seq (cons [m1 m2] (stops-to-pairs ms)))
                    true
                    (lazy-seq (cons [m1 nil] (stops-to-pairs (cons m2 ms)))))
            (is-platform m1)
              (lazy-seq (cons [nil m1] (stops-to-pairs (cons m2 ms)))))))


(defn- members-groups [members]
  "Split members to groups of stops+platforms, rods and erroneous members"
  (let [member-type->group {:stop     :stops-and-platforms
                            :platform :stops-and-platforms
                            :road     :roads}
        parse-fsm {[:stops-and-platforms :stops-and-platforms] :stops-and-platforms
                   [:stops-and-platforms :roads]               :roads
                   [:stops-and-platforms nil]                  :stops-and-platforms
                   [:roads               :stops-and-platforms] nil
                   [:roads               :roads]               :roads
                   [:roads               nil]                  :roads}
        member-group (comp member-type->group member-type)]
        (loop [[m & ms] members
               parse-state :stops-and-platforms
               stops-and-platforms []
               roads []
               erroneous []]
          (if m
            (let [g (member-group m)]
              (recur ms
                     (parse-fsm [parse-state g])
                     (if (= g :stops-and-platforms)
                       (conj stops-and-platforms m) stops-and-platforms)
                     (if (= g :roads)
                       (conj roads m) roads)
                     (if (nil? g)
                       (conj erroneous m) erroneous)))
            {:stops-and-platforms stops-and-platforms
             :roads roads
             :erroneous erroneous
             :invalid-order? (nil? parse-state)}))))

(defn- parse-stops [relation-members]
  )

(defn analyze-route-variant [osm-data relation-id]
  )

