(ns ptwatch.osm-xml-test
  (:use [clojure.test])
  (:require [ptwatch.osm]))

(defn- string-to-stream [string]
  (-> string .trim .getBytes java.io.ByteArrayInputStream.))

(defn- xml-parse-string [string]
  (clojure.xml/parse (string-to-stream string)))

(def ^{:private true} testxml-src "
<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<osm version='0.6' generator='CGImap 0.0.2'>
 <bounds minlat='54.0889580' minlon='12.2487570' maxlat='54.0913900' maxlon='12.2524800'/>
 <node id='298884269' lat='54.0901746' lon='12.2482632' user='SvenHRO' uid='46882' visible='true' version='1' changeset='676636' timestamp='2008-09-21T21:37:45Z'/>
 <node id='261728686' lat='54.0906309' lon='12.2441924' user='PikoWinter' uid='36744' visible='true' version='1' changeset='323878' timestamp='2008-05-03T13:39:23Z'/>
 <node id='1831881213' version='1' changeset='12370172' lat='54.0900666' lon='12.2539381' user='lafkor' uid='75625' visible='true' timestamp='2012-07-20T09:43:19Z'>
  <tag k='name' v='Neu Broderstorf'/>
  <tag k='traffic_sign' v='city_limit'/>
 </node>
 <node id='298884272' lat='54.0901447' lon='12.2516513' user='SvenHRO' uid='46882' visible='true' version='1' changeset='676636' timestamp='2008-09-21T21:37:45Z'/>
 <way id='26659127' user='Masch' uid='55988' visible='true' version='5' changeset='4142606' timestamp='2010-03-16T11:47:08Z'>
  <nd ref='298884269'/>
  <nd ref='261728686'/>
  <tag k='highway' v='unclassified'/>
  <tag k='name' v='Test street'/>
 </way>
 <relation id='56688' user='kmvar' uid='56190' visible='true' version='28' changeset='6947637' timestamp='2011-01-12T14:23:49Z'>
  <member type='node' ref='298884269' role='role1'/>
  <member type='node' ref='298884272' role=''/>
  <member type='way' ref='26659127' role='role2'/>
  <member type='node' ref='261728686' role=''/>
  <tag k='name' v='Kustenbus Linie 123'/>
  <tag k='network' v='VVW'/>
  <tag k='operator' v='Regionalverkehr Kuste'/>
  <tag k='ref' v='123'/>
  <tag k='route' v='bus'/>
  <tag k='type' v='route'/>
 </relation>
</osm>
  ")

(defn- testxml [] (string-to-stream testxml-src))

(deftest test-parse-tags
  (is (= (#'ptwatch.osm/parse-tags (xml-parse-string "
    <node>
      <tag k=\"name\" v=\"Neu Broderstorf\"/>
      <glug/>
      <tag k=\"traffic_sign\" v=\"city_limit\"/>
    </node>"))
    {"name" "Neu Broderstorf", "traffic_sign" "city_limit"}
  )))

(deftest test-parse-xml
  (is (= (ptwatch.osm/parse-xml (testxml))
        [{:type :node :id 298884269 :lat 54.0901746 :lon 12.2482632}
         {:type :node :id 261728686 :lat 54.0906309 :lon 12.2441924}
         {:type :node :id 1831881213 :lat 54.0900666 :lon 12.2539381
          "name" "Neu Broderstorf" "traffic_sign" "city_limit"}
         {:type :node :id 298884272 :lat 54.0901447 :lon 12.2516513}
         {:type :way :id 26659127 :nodes [298884269 261728686]
          "highway" "unclassified" "name" "Test street"}
         {:type :relation
          :id 56688
          "name" "Kustenbus Linie 123"
          "network" "VVW"
          "operator" "Regionalverkehr Kuste"
          "ref" "123"
          "route" "bus"
          "type" "route"
          :members [{:type :node :ref 298884269 :role "role1"}
                    {:type :node :ref 298884272 :role ""}
                    {:type :way :ref 26659127 :role "role2"}
                    {:type :node :ref 261728686 :role ""}]}])))

(deftest test-store
  (is (= (-> (testxml) ptwatch.osm/parse-xml ptwatch.osm/store-primitives)
        {[:node 298884269]  {:type :node :id 298884269
                             :lat 54.0901746 :lon 12.2482632}
         [:node 261728686]  {:type :node :id 261728686
                             :lat 54.0906309 :lon 12.2441924}
         [:node 1831881213] {:type :node :id 1831881213
                             :lat 54.0900666 :lon 12.2539381
                             "name" "Neu Broderstorf"
                             "traffic_sign" "city_limit"}
         [:node 298884272]  {:type :node :id 298884272
                             :lat 54.0901447 :lon 12.2516513}
         [:way 26659127]    {:type :way :id 26659127
                             :nodes [298884269 261728686]
                             "highway" "unclassified"
                             "name" "Test street"}
         [:relation 56688]  {:type :relation :id 56688
                             "name" "Kustenbus Linie 123"
                             "network" "VVW"
                             "operator" "Regionalverkehr Kuste"
                             "ref" "123"
                             "route" "bus"
                             "type" "route"
                             :members [{:type :node :ref 298884269
                                        :role "role1"}
                                       {:type :node :ref 298884272
                                        :role ""}
                                       {:type :way :ref 26659127
                                        :role "role2"}
                                       {:type :node :ref 261728686
                                        :role ""}]}})))

(deftest test-deref-rel-members
  (let [store (-> (testxml) ptwatch.osm/parse-xml ptwatch.osm/store-primitives)
        rel (store [:relation 56688])
        rel-members (rel :members)
        expected [{:type :node :id 298884269 :lat 54.0901746 :lon 12.2482632 :role "role1"}
                  {:type :node :id 298884272 :lat 54.0901447 :lon 12.2516513 :role ""}
                  {:type :way :id 26659127 :nodes [298884269 261728686]
                   "highway" "unclassified" "name" "Test street"
                   :role "role2"}
                  {:type :node :id 261728686 :lat 54.0906309 :lon 12.2441924 :role ""}]]
    (is (= (ptwatch.osm/deref-rel-members store rel) expected))
    (is (= (ptwatch.osm/deref-rel-members store rel-members) expected))))
