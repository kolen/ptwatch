(ns ptwatch.analyze-test
  [:use clojure.test]
  [:use ptwatch.analyze]
  [:use ptwatch.osm])

(def test-data-1 (ptwatch.osm/store-primitives
                  [{:id 128378 :type :node :lat 31.337 :lon 12.345}
                   {:id 873463 :type :node :lat 38.483 :lon 18.483}
                   {:id 283742 :type :way :nodes [128378 873463]}]))

(deftest test-members-groups
  (are [members result]
       (= (#'ptwatch.analyze/members-groups members) result)

       [{:ref 128378 :type :node :role "stop" }
        {:ref 873463 :type :node :role "stop_exit_only"}
        {:ref 283742 :type :way :role ""}]

       {:stops_and_platforms
        [{:ref 128378 :type :node :role "stop" }
         {:ref 873463 :type :node :role "stop_exit_only"}]
        :roads
        [{:ref 283742 :type :way :role ""}]}))
