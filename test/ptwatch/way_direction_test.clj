(ns ptwatch.way-direction-test
  [:use clojure.test]
  [:use ptwatch.way-direction])

(defn generate-many-ways [n]
  (take n (for [nodes (partition 10 9 (range))] {:nodes (vec nodes)})))

(deftest test-way-direction
  (are [dir pts ways] (= dir (way-direction pts ways))
       :forward  #{1 2} [{:nodes [1 2 3 4 5]}]
       :backward nil    [{:nodes [3 4 5]}
                         {:nodes [1 2 3]}]
       :forward  nil    [{:nodes [1 2 3 4] "oneway" "yes"}]
       :unknown  nil    [{:nodes [1 2 3]}
                         {:nodes [3 4 5 6 1]}
                         {:nodes [3 8 9 10 1]}]
       :forward  nil    [{:nodes [1 2 3 4]}
                         {:nodes [4 5 6 1]}
                         {:nodes [1 6 7 8]}]
       :forward  nil    [{:nodes [1 2 3 4]}
                         {:nodes [4 5 7]}
                         {:nodes [10 11 12]}]
       :unknown  nil    [{:nodes [1 2 3]}
                         {:nodes [5 6 7]}]
       nil       #{1 2} [{:nodes [4 5 6]}]
       :backward nil    [{:nodes [1 2 3]}
                         {:nodes [3 5 1]}
                         {:nodes [3 4 5 1] "oneway" "1"}
                         {:nodes [7 8 9]}]
       :forward  nil    [{:nodes [1 2 3]}
                         {:nodes [3 5 1]}
                         {:nodes [3 4 5 1] "oneway" "reverse"}
                         {:nodes [10 11 12]}]
       :unknown  nil    [{:nodes [1 2 3]}
                         {:nodes [3 4 5] "oneway" "reverse"}]
       :forward  nil (generate-many-ways 10000)))

(deftest test-ways-directions
  (are [ways result] (= (ways-directions ways) result)

       [{:nodes [1 2 3 4 5 6]} {:nodes [10 9 8 7 6]} {:nodes [10 11]}]
       [[:forward {:nodes [1 2 3 4 5 6]}]
        [:backward {:nodes [10 9 8 7 6]}]
        [:forward {:nodes [10 11]}]]

       [{:nodes [1 2 3]}]
       [[:unknown {:nodes [1 2 3]}]]

       [{:nodes [1 2 3 4]}
        {:nodes [4 5 6 1]}
        {:nodes [1 9 10 4]}
        {:nodes [5 6 7]}]
       [[:unknown {:nodes [1 2 3 4]}]
        [:unknown {:nodes [4 5 6 1]}]
        [:unknown {:nodes [1 9 10 4]}]
        :break
        [:unknown {:nodes [5 6 7]}]]

       []
       []))

(run-tests)
